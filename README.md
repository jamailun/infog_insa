# InfoG - ROSAZ & HUE

Projet de 4INFO de l'INSA Rennes, en informatique graphique. Année 2021-2022.

Il s'agit d'une petite cinématique avec un sous-marin et un poisson lanceur de torpilles. C'est un projet scolaire, donc une grande partie du code n'est pas de moi.
Choses rajoutées :
- Cylindre texturé,
- Animations de caméra,
- Shaders pour l'eau et les bulles,
- Système de scène un peu étrange mais sympa.

## Auteurs

- Timothé ROSAZ
- Steven HUE

## Informations supplémentaires

Le projet est actuellement "configuré" pour tourner sous Windows, et ne **possède pas** de librairies. Il faut ajouter les bonnes puis sélectionner le bon `CMakeList` (en le renommant) selon l'OS souhaité.

## Remerciements

- Merci à Louis FRÉNEAU pour la [vidéo montée](https://youtu.be/1T8lNcUSRWk).

#!/bin/sh

cd sfmlGraphicsPipeline/build
echo Build pipeline...
make -j6 || exit 1
cd ../../

cd ouifProject/build
echo Build project...
make -j6 || exit 1

echo All build are over.

echo Execute main.
./main


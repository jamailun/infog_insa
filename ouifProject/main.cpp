#include <Viewer.hpp>
#include <Camera.hpp>
#include <CameraAnimation.hpp>
#include <ShaderProgram.hpp>
#include <FrameRenderable.hpp>

#include <texturing/TexturedPlaneRenderable.hpp>
#include <texturing/TexturedCubeRenderable.hpp>
#include <texturing/MultiTexturedCubeRenderable.hpp>
#include <texturing/TexturedLightedMeshRenderable.hpp>
#include <texturing/MipMapCubeRenderable.hpp>
#include <texturing/BillBoardPlaneRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <texturing/TexturedTriangleRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <lighting/Light.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/Material.hpp>

#include <iostream>
#include <vector>

#define DEBUG false

#include "scenes/Scene_00.hpp"
#include "scenes/Scene_01.hpp"
#include "scenes/Scene_02.hpp"
#include "scenes/Scene_03.hpp"
#include "scenes/Scene_04.hpp"
#include "scenes/Scene_05.hpp"

#define FIRST_SCENE 5

ShaderProgramPtr createShader(std::string prefix) {
    return std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/"+prefix+"Vertex.glsl", "../../sfmlGraphicsPipeline/shaders/"+prefix+"Fragment.glsl");
}

void callback_scenes(int sceneID);

Viewer* g_viewer;
Scene* currentScene;
ShaderProgramPtr SHADERS[6];

int main()  {
	Viewer viewer(1280,720);
    g_viewer = &viewer;
    SHADERS[SHADER_FULL] = createShader("texture");
    SHADERS[SHADER_FLAT] = createShader("flat");
    SHADERS[SHADER_NORM] = createShader("norm");
    SHADERS[SHADER_TEXT] = createShader("simpleTexture");
    SHADERS[SHADER_BUBL] = createShader("bubble");
    SHADERS[SHADER_WATR] = createShader("water");
    for(int i = SHADER_FULL; i <= SHADER_WATR; i++)
        viewer.addShaderProgram(SHADERS[i]);

    viewer.getCamera().setMouseBehavior( Camera::SPACESHIP_BEHAVIOR );
    viewer.getCamera().setZfar(300); // it's 100 by default

    callback_scenes(FIRST_SCENE-1);
    
	while( viewer.isRunning() ) {
	    viewer.handleEvent();
		viewer.animate();
		viewer.draw();
		viewer.display();
		viewer.testCallback();
        if(currentScene != nullptr)
            currentScene->newTime(viewer.getTime());
	}	

	return EXIT_SUCCESS;
}

// Tableau des scènes utilisées dans le programme
Scene* SCENES_ARRAY[] = {
    new Scene_00(callback_scenes), // Scène 1 : introduction du sous-marin sur un trébuchet
    new Scene_01(callback_scenes), // Scène 2 : amerissage du sous-marin dans l'eau
    new Scene_02(callback_scenes), // Scène 3 : le sous-marin descend dans les profondeurs
    new Scene_03(callback_scenes), // Scène 4 : le sous-marin entre dans le temple
    new Scene_04(callback_scenes), // Scène 5 : longue scène avec le poisson
    new Scene_05(callback_scenes)  // Scène pour les tests
}; 

// Méthode appellée par les callbacks des scènes
void callback_scenes(int sceneID) {
    // Nettoyage du Viewer
    g_viewer->stopAnimation();
    g_viewer->clearRenderables();
    g_viewer->resetLights(); // nous n'avons pas réussi à faire fonctionner le nettoyage des lumières
    g_viewer->getCamera().clearAnimations();
    g_viewer->resetAnimation();

    std::cout << "Data cleared. Start scene "<<(sceneID+1)<<".\n";

    // Affectation de la nouvelle scène, et chargement de celle-ci.
    currentScene = SCENES_ARRAY[sceneID + 1];
    currentScene->loadAndStart(*g_viewer, SHADERS);
}

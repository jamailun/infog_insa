#include "Factory.hpp"

#include <lighting/Material.hpp>

inline std::string getMesh(std::string mesh) {
    return "./../meshes/" + mesh;
}
inline std::string getTexture(std::string texture) {
    return "./../textures/" + texture;
}
inline std::string getShader(std::string shader) {
    return "./../../sfmlGraphicsPipeline/shaders/" + shader;
}

// SUBMARINE

void rotatePeriscope(Submarine& sub, float from, float duration) {
    sub.periscope->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.0f),   glm::vec3(0, 1, 0))), from);
    sub.periscope->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(180.0f), glm::vec3(0, 1, 0))), from + (duration/2));
    sub.periscope->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(360.0f), glm::vec3(0, 1, 0))), from + duration);
}

void rotateFan(Submarine& sub, float from, float duration, float speed) {
    float incr = 1.0f / speed;
    for(float t = from; t <= from + duration; t += incr) {
        sub.fan->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.0f),   glm::vec3(0, 0, 1))), t+0.0001f);
        sub.fan->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(180.0f), glm::vec3(0, 0, 1))), t+(incr/2.0f));
        sub.fan->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(360.0f), glm::vec3(0, 0, 1))), t+incr-0.0001f);
    }
}

Submarine createSubmarine(ShaderProgramPtr& shader, Viewer& viewer) {
    Submarine sub;
    MaterialPtr textureMaterial = Material::TextureMaterial();

    // Container
    sub.container = std::make_shared<ContainerRenderable>(shader);

    // Main part
    sub.main = std::make_shared<FullRenderable>(shader, getMesh("sub/mainbody.obj"), getTexture("sub/mainbody_color.png"));
    sub.main->setMaterial(textureMaterial);

    // Periscope
    sub.periscope = std::make_shared<FullRenderable>(shader, getMesh("sub/peri.obj"), getTexture("sub/mainbody_color.png"));
    glm::mat4 periTransform(1);
    sub.periscope->setParentTransform(periTransform);
    sub.periscope->setMaterial(textureMaterial);

    // Fan
    sub.fan = std::make_shared<FullRenderable>(shader, getMesh("sub/fan.obj"), getTexture("sub/mainbody_color.png"));
    glm::mat4 fanTransform(1);
    fanTransform = glm::translate(fanTransform, glm::vec3(0,0,0));
    sub.fan->setParentTransform(fanTransform);
    sub.fan->setMaterial(textureMaterial);
    
    // Hierarchical  settings
    HierarchicalRenderable::addChild(sub.container, sub.main);
    HierarchicalRenderable::addChild(sub.main, sub.periscope);
    HierarchicalRenderable::addChild(sub.main, sub.fan);

    // Add to renderable
    viewer.addRenderable(sub.container);

    return sub;
}

// FISH

void twerk(Fish& fish, float from, float duration, int amount) {
    float cycleDuration = (duration / amount);
    float biCycleDuration = cycleDuration / 2.0f;
    bool pair = false;
    for(float i = from; i < (duration+from); i += cycleDuration) {
        pair = ! pair;
        fish.assL->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0)), i);
        fish.assL->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0, pair ? 0.5 : -0.5 ,0)), i + biCycleDuration -0.0001f);
        fish.assR->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0)), i);
        fish.assR->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0, pair ? -0.5 : 0.5 ,0)), i + biCycleDuration -0.0001f);
    }
    fish.assL->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0)), duration+from);
    fish.assR->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0)), duration+from);
}

void openMouth(Fish& fish, float from, float duration, int amount) {
    float cycleDuration = (duration / amount);
    float biCycleDuration = cycleDuration / 2.0f;
    for(float i = from; i <= (duration+from); i += cycleDuration) {
        fish.jaw->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.0f), glm::vec3(1, 0, 0))), i);
        fish.jaw->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(20.0f), glm::vec3(0, 0, 1))), i+biCycleDuration-0.0001f);
    }
}

Fish createFish(ShaderProgramPtr& shader, Viewer& viewer) {
    Fish fish;
    MaterialPtr textureMaterial = Material::TextureMaterial();

    fish.body = std::make_shared<FullRenderable>(shader, getMesh("fish/body.obj"), getTexture("fish/body_texture.png"));
    fish.body->setMaterial(textureMaterial);
    viewer.addRenderable(fish.body);
    
    fish.assL = std::make_shared<FullRenderable>(shader, getMesh("fish/assL.obj"), getTexture("fish/Ass_Base_color.png"));
    fish.assL->setMaterial(textureMaterial);
    HierarchicalRenderable::addChild(fish.body, fish.assL);

    fish.assR = std::make_shared<FullRenderable>(shader, getMesh("fish/assR.obj"), getTexture("fish/Ass_Base_color.png"));
    fish.assR->setMaterial(textureMaterial);
    HierarchicalRenderable::addChild(fish.body, fish.assR);
    
    fish.lantern = std::make_shared<FullRenderable>(shader, getMesh("fish/lantern.obj"), getTexture("fish/lantern_texture.png"));
    fish.lantern->setMaterial(textureMaterial);
    HierarchicalRenderable::addChild(fish.body, fish.lantern);
    
    fish.jaw = std::make_shared<FullRenderable>(shader, getMesh("fish/jaw.obj"), getTexture("fish/jaw_texture.png"));
    fish.jaw->setMaterial(textureMaterial);
    HierarchicalRenderable::addChild(fish.body, fish.jaw);

    return fish;
}


// PARTICLE SYSTEM

ParticleSystem initParticleSystem(Viewer& viewer, glm::vec3 constantForce) {
    ParticleSystem ps;
    ps.system = std::make_shared<DynamicSystem>();
    ps.system->setSolver(std::make_shared<EulerExplicitSolver>());
    ps.system->setDt(0.001);
    ps.system->setCollisionsDetection(false);
    if(constantForce != glm::vec3(0))
        ps.system->addRealConstantForceField(std::make_shared<RealConstantForceField>(constantForce));
    ps.renderer = std::make_shared<DynamicSystemRenderable>(ps.system);
    viewer.addRenderable(ps.renderer);
    return ps;
}

void createParticle(ShaderProgramPtr& shader, ParticleSystem& ps, glm::vec3 position, glm::vec3 velocity, float radius, float mass) {
    // Particle
    ParticlePtr particle = std::make_shared<Particle>(position, velocity, mass, radius);
    ps.system->addParticle(particle);
    // Renderable
    ParticleRenderablePtr particleRenderable = std::make_shared<ParticleRenderable>(shader, particle);
    HierarchicalRenderable::addChild(ps.renderer, particleRenderable);
}

// ALGUES

FullRenderablePtr createAlgue(ShaderProgramPtr& shader, Viewer& viewer, glm::vec3 position) {
    FullRenderablePtr algue = std::make_shared<FullRenderable>(shader, getMesh("algue.obj"), getTexture("algue_texture.png"));
    algue->setMaterial(Material::TextureMaterial());
    viewer.addRenderable(algue);
    for(float i = 0; i <= 20; i += 4) {
        algue->addLocalTransformKeyframe(GeometricTransformation(position, glm::quat(glm::vec3{0.5,0,0}), glm::vec3(3)), i);
        algue->addLocalTransformKeyframe(GeometricTransformation(position, glm::quat(glm::vec3{-0.5,0,0}), glm::vec3(3)), i + 2);
    }
    return algue;
}

// MISSILE

void rotateMissileFan(Missile& m, float from, float duration, float speed) {
    float incr = 1.0f / speed;
    for(float t = from; t <= from + duration; t += incr) {
        m.fan->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.0f),   glm::vec3(0, 0, 1))), t+0.0001f);
        m.fan->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(180.0f), glm::vec3(0, 0, 1))), t+(incr/2.0f));
        m.fan->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(360.0f), glm::vec3(0, 0, 1))), t+incr-0.0001f);
    }
}

Missile createMissile(ShaderProgramPtr& shader, Viewer& viewer) {
    Missile missile;

    missile.decalage_fan = glm::vec3(0.05,-0.65,1.28);
    missile.scale_fan = glm::vec3(0.5);

    missile.body = std::make_shared<FullRenderable>(shader, getMesh("fish/missile.obj"), getTexture("fish/Missile_Base_color.png"));
    viewer.addRenderable(missile.body);

    missile.fan = std::make_shared<FullRenderable>(shader, getMesh("sub/fan.obj"), getTexture("sub/mainbody_color.png"));
    glm::mat4 fan_transform(1);
    fan_transform = glm::translate(fan_transform, missile.decalage_fan);
    fan_transform = glm::scale(fan_transform, missile.scale_fan);
    missile.fan->setParentTransform(fan_transform);
    HierarchicalRenderable::addChild(missile.body, missile.fan);

    return missile;
}

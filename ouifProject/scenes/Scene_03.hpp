#pragma once

#include "Scene.hpp"
#include <Utils.hpp>
#include <FullRenderable.hpp>
#include <FrameRenderable.hpp>
#include <lighting/LightedMeshRenderable.hpp>
#include <lighting/Material.hpp>
#include <lighting/Light.hpp>
#include <lighting/SpotLightRenderable.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <texturing/TexturedCylinderRenderable.hpp>
#include <texturing/TexturedCubeRenderable.hpp>
#include <CameraAnimation.hpp>
#include <iostream>

#include "../Factory.hpp"

class Scene_03 : public Scene {
public:
    Scene_03(std::function<void(int)> callback) : Scene(callback) {}
    
    void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) {
        float SCENE_END = 14;

        // Light source
        DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(
            /* Direction : */ glm::normalize(glm::vec3(0.0, -1.0, -2.0)),
            /* Ambiant :   */ glm::vec3(0.388, 0.286, 0.995),
            /* Diffuse :   */ glm::vec3(0.1,0.1,0.5),
            /* Specular :  */ glm::vec3(0.05,0.05,0.25)
        );
        viewer.setDirectionalLight(directionalLight);
        MaterialPtr textureMaterial = Material::TextureMaterial();

        particles = initParticleSystem(viewer, glm::vec3{0,60,0});
        shaderBubble = shaders[SHADER_BUBL];
        nextBubble = 2;
        STOP_BUBBLES = SCENE_END - 2;

        // Temple
        auto temple = std::make_shared<FullRenderable>(shaders[SHADER_FULL], getMesh("temple/temple.obj"), getTexture("temple/temple_color.png"));
        glm::mat4 temple_transform = temple->getModelMatrix();
        temple_transform = glm::rotate(temple_transform, glm::radians(3.f), glm::vec3(1, 0, 0));
        temple_transform = glm::translate(temple_transform, glm::vec3(0,-5,-12));
        temple_transform = glm::scale(temple_transform, glm::vec3(2));
        temple->setParentTransform(temple_transform);
        viewer.addRenderable(temple);
        // Lumière sortant du temple
        lightTemple = std::make_shared<PointLight>(
            glm::vec3{-2, -6, 15}, // position,
            glm::vec3(1,1,0.1),    // ambient color
            glm::vec3(0.2),        // diffuse
            glm::vec3(0.5),        // specular
            0.03,                  // constant attenuation
            0.02,                  // linear attenuation
            0.001                  // quadratic attenuation
        );
        viewer.addPointLight(lightTemple);
        viewer.addRenderable(std::make_shared<PointLightRenderable>(shaders[SHADER_FLAT], lightTemple));

        // Submarine
        submarine = createSubmarine(shaders[SHADER_FULL], viewer);
        glm::quat orientation = glm::quat_cast(glm::lookAt(glm::vec3(70,10,20), glm::vec3(40,30,20), glm::vec3(0,0,1)));
        rotateFan(submarine, 0, SCENE_END, 1.8);

        // Contour
        auto fond = std::make_shared<TexturedCylinderRenderable>(shaders[SHADER_TEXT], getTexture("underwater_1.jpg"));
        glm::mat4 fond_transform(1);
        fond_transform = glm::translate(fond_transform, glm::vec3(0,-50,0));
        fond_transform = glm::scale(fond_transform, glm::vec3(130,120,130));
        fond_transform = glm::rotate(fond_transform, -1.57079f, glm::vec3(1,0,0));
        fond->setParentTransform(fond_transform);
        viewer.addRenderable(fond);

        // Fond marin
        auto ground = std::make_shared<FullRenderable>(shaders[SHADER_FULL], getMesh("ground_water.obj"), getTexture("ground_texture.png"));
        ground->setMaterial(Material::TextureMaterial());
        glm::mat4 ground_transform(1);
        ground_transform = glm::translate(ground_transform, glm::vec3(0,-27,0));
        ground_transform = glm::scale(ground_transform, glm::vec3(16));
        ground->setParentTransform(ground_transform);
        viewer.addRenderable(ground);

        // Algues
        glm::vec3 algues[] = {
            glm::vec3(21.35, -22.22, 76.27),
            glm::vec3(53.6412, -23.9576, 32.1124),
            glm::vec3(12.4055, -23.4144, 64.7476),
            glm::vec3(4.93733, -23.5679, 72.6436)
        };
        for(int i = 0; i < 4; i++)
            createAlgue(shaders[SHADER_FULL], viewer, algues[i]);
        
        // Caméra (ce coup ci on fait du plan fixe)
        viewer.getCamera().setViewMatrix(glm::lookAt(glm::vec3(90,40,90), glm::vec3(10,-15,0), glm::vec3(0,1,0)));

        // Mouvements du sous-marin
        glm::mat4 o_1 = glm::lookAt(glm::vec3(125,30,20), glm::vec3(80,4,48), glm::vec3(0,1,0));
        glm::mat4 o_2 = glm::lookAt(glm::vec3(80,4,48), glm::vec3(11,-8,53), glm::vec3(0,1,0));
        glm::mat4 o_3 = glm::lookAt(glm::vec3(11,-8,53), glm::vec3(-2,-9,50), glm::vec3(0,1,0));
        glm::mat4 o_4 = glm::lookAt(glm::vec3(-4,-9,2.8), glm::vec3(-2,-9,50), glm::vec3(0,1,0));
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(125,30,20), glm::quat(o_1)), 0);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(80,4,48), glm::quat(o_2)), 4);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(11,-8,53), glm::quat(o_3)), 8);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(-2,-9,50), glm::quat(o_4)), 10);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(-4,-9,2.8), glm::quat(o_4)), 13);

        //  Finalization
        viewer.startAnimation();
        enableCallback(viewer, 3, SCENE_END);
    }

    void newTime(float time) {
        if(time >= nextBubble && time <= STOP_BUBBLES ) {
            float mass = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            createParticle(shaderBubble, particles, glm::vec3(submarine.fan->getModelMatrix()[3]), glm::vec3{0,0,mass*3}, mass * 0.5, mass * 0.2);
            float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            nextBubble += (r / 1.2);
        }
    }

    void over() {
        std::cout << "lum="<<lightTemple->linear()<<"\n";
        lightTemple->setAmbient(glm::vec3(0));
        lightTemple->setLinear(9999);
        std::cout << "lum="<<lightTemple->linear()<<"\n";
    }

private:
    // Light to reset
    PointLightPtr lightTemple;
    // Bubbles
    float STOP_BUBBLES = 20;
    float nextBubble = 99999;
    ParticleSystem particles;
    Submarine submarine;
    ShaderProgramPtr shaderBubble;
};

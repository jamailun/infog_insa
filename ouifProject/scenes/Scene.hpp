#ifndef SCENE_HPP
#define SCENE_HPP

#include <Viewer.hpp>
#include <ShaderProgram.hpp>
#include <functional>
#include <string>
#include <iostream>
using namespace std::placeholders;
using std::string;

#define SHADER_FULL 0
#define SHADER_FLAT 1
#define SHADER_NORM 2
#define SHADER_TEXT 3
#define SHADER_BUBL 4
#define SHADER_WATR 5

class CallBackScene : public CallBack {
    public:
        CallBackScene(std::function<void(int)> callback_method, int id) : CallBack() {
            m_callback_method = callback_method;
            m_id = id;
        }
    protected:
        void apply() {
            std::cout << "End of scene "<<m_id<<", call main.\n";
            m_callback_method(m_id);
        }
    private:
        std::function<void(int)> m_callback_method;
        int m_id;
};

class Scene {
    public:
        Scene(std::function<void(int)> callback) {
            m_callback_method = callback;
        }
        ~Scene() {
            //delete m_callback; does segfault :(
        }
        virtual void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) = 0;
        virtual void newTime(float time) {
            // To override if needed
        }
    protected:
        inline string getMesh(string mesh) const {
            return "./../meshes/" + mesh;
        }
        inline string getTexture(string texture) const {
            return "./../textures/" + texture;
        }
        inline string getShader(string shader) const {
            return "./../../sfmlGraphicsPipeline/shaders/" + shader;
        }
        void enableCallback(Viewer& viewer, int id, float time) {
            if(DEBUG)
                time = 2;
            m_callback = new CallBackScene(m_callback_method, id);
            viewer.setCallback(m_callback, time);
        }
    protected:
        friend CallBackScene;
        std::function<void(int)> m_callback_method;
        CallBackScene* m_callback = nullptr;
};

#endif
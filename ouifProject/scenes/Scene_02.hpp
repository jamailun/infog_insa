#pragma once

#include <stdlib.h> // srand(), rand()
#include <time.h>   // time()

#include "Scene.hpp"
#include <CubeRenderable.hpp>
#include <FullRenderable.hpp>
#include <lighting/LightedMeshRenderable.hpp>
#include <lighting/Material.hpp>
#include <lighting/Light.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <CameraAnimation.hpp>
#include <iostream>
#include "../Factory.hpp"

class Scene_02 : public Scene {
public:
    Scene_02(std::function<void(int)> callback) : Scene(callback) {
        srand(time(NULL));
    }
    
    void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) {
        // Light source
        DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(
            /* Direction : */ glm::normalize(glm::vec3(0.0,-1.0,-1.0)),
            /* Ambiant :   */ glm::vec3(0.788, 0.886, 1.0),
            /* Diffuse :   */ glm::vec3(0.3,0.3,0.1),
            /* Specular :  */ glm::vec3(0.3,0.3,0.1)
        );
        viewer.setDirectionalLight(directionalLight);
        
        particles = initParticleSystem(viewer, glm::vec3{0,12,0});
        shaderBubble = shaders[SHADER_BUBL];

        // fond
        auto fond = std::make_shared<TexturedPlaneRenderable>(shaders[SHADER_TEXT], getTexture("underwater_0.jpg"));
        glm::mat4 fond_transform(1);
        fond_transform = glm::translate(fond_transform, glm::vec3(7,-12,-16));
        fond_transform = glm::scale(fond_transform, glm::vec3(1,50,80));
        fond_transform = glm::rotate(fond_transform, -1.57079f, glm::vec3(0,1,0));
        fond->setParentTransform(fond_transform);
        viewer.addRenderable(fond);

        // sous-marin
        submarine = createSubmarine(shaders[SHADER_FULL], viewer);
        float subScale = 0.45f;
        // initial state
        submarine.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(180.f), glm::vec3(0,1,0)), glm::vec3(subScale)), 0);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.f),  glm::vec3(1,0,0))), 0);
        rotateFan(submarine, 0.f, 1.2f, 0.4f);
        // commence à descendre
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,-0.5f,-0.5f), glm::angleAxis(glm::radians(-30.f),  glm::vec3(1,0,0))), 2);
        rotatePeriscope(submarine, 8.f, 1.2f);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,-20.1f,-20.1f), glm::angleAxis(glm::radians(-50.f),  glm::vec3(1,0,0))), 9);
        rotateFan(submarine, 1.3f, 25, 1.7f);
        // partir au loin
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,-70,-90), glm::angleAxis(glm::radians(10.0f), glm::vec3(1,0,0))), 30);

        // Camera
        viewer.getCamera().setViewMatrix(glm::lookAt(glm::vec3(-15,0,0), glm::vec3(0), glm::vec3(0,1,0)));
        auto traveling_1 = new MonoTravelingAnimation(glm::vec3(-15,0,0), glm::vec3(-15,-20,-20), glm::vec3(1,0,0), 3, 8);
        auto traveling_2 = new MonoTravelingAnimation(glm::vec3(-15,-20,-20), glm::vec3(-15,-20.2,-20.2), glm::vec3(1,0,0), 8, 9);
        viewer.getCamera().addAnimation(traveling_1);
        viewer.getCamera().addAnimation(traveling_2);
        viewer.startAnimation();
        
        nextBubble = 2;
        enableCallback(viewer, 2, 14);
    }

    void newTime(float time) {
        if(time >= nextBubble) {
            float mass = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            createParticle(shaderBubble, particles, glm::vec3(submarine.fan->getModelMatrix()[3]), glm::vec3{0,0,mass*3}, mass * 0.5, mass * 0.2);
            float r = static_cast <float> (rand()) / static_cast <float> (RAND_MAX);
            nextBubble += (r / 1.2);
        }
    }
private:
    float nextBubble = 99999;
    ParticleSystem particles;
    Submarine submarine;
    ShaderProgramPtr shaderBubble;
};

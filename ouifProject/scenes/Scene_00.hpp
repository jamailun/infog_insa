#pragma once

#include "Scene.hpp"
#include <Viewer.hpp>
#include <FullRenderable.hpp>
#include <KeyframedCylinderRenderable.hpp>
#include <lighting/LightedMeshRenderable.hpp>
#include <lighting/Material.hpp>
#include <lighting/Light.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <texturing/TexturedCylinderRenderable.hpp>
#include <CameraAnimation.hpp>

#include "../Factory.hpp"

class Scene_00 : public Scene {
public:
    Scene_00(std::function<void(int)> callback) : Scene(callback) {}
    
    void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) {
        float TREBU_START = 8.0f;
        float SCENE_END = TREBU_START + 9;
        float SPEEDZ = 0.15;

        // Light source
        viewer.setDirectionalLight(std::make_shared<DirectionalLight>(
            /* Direction : */ glm::normalize(glm::vec3(0.0,-1.0,-1.0)),
            /* Ambiant :   */ glm::vec3(0.788, 0.886, 1.0),
            /* Diffuse :   */ glm::vec3(0.3,0.3,0.1),
            /* Specular :  */ glm::vec3(0.3,0.3,0.1)
        ));

        MaterialPtr textureMaterial = Material::TextureMaterial();

        // Ciel
        auto ciel = std::make_shared<TexturedCylinderRenderable>(shaders[SHADER_TEXT], getTexture("ciel.png"));
        glm::mat4 ciel_transform(1);
        ciel_transform = glm::translate(ciel_transform, glm::vec3(0,-35,0));
        ciel_transform = glm::scale(ciel_transform, glm::vec3(50,90,50));
        ciel_transform = glm::rotate(ciel_transform, -1.57079f, glm::vec3(1,0,0));
        ciel->setParentTransform(ciel_transform);
        viewer.addRenderable(ciel);

        // Hill
        auto hill = std::make_shared<FullRenderable>(shaders[SHADER_FULL], getMesh("hill.obj"), getTexture("HillTexture.png"));
        glm::mat4 hillTransform(1);
        hillTransform = glm::translate(hillTransform, glm::vec3(14,-34,3));
        hillTransform = glm::scale(hillTransform, glm::vec3(2,2,2));
        hill->setParentTransform(hillTransform);
        hill->setMaterial(textureMaterial);
        viewer.addRenderable(hill);

        // Trébuchet
        auto trebuchet_1 = std::make_shared<FullRenderable>(shaders[SHADER_FULL], getMesh("trebuchet_1.obj"), getTexture("text_trebuchet_1.png"));
        auto trebuchet_2 = std::make_shared<FullRenderable>(shaders[SHADER_FULL], getMesh("trebuchet_2.obj"), getTexture("text_trebuchet_2.png"));
        trebuchet_1.get()->setMaterial(textureMaterial);
        trebuchet_2.get()->setMaterial(textureMaterial);
        glm::mat4 transform(1);
        transform = glm::translate(transform, glm::vec3(-1.3,2.05,-1.2));
        trebuchet_2.get()->setParentTransform(transform);

        trebuchet_2.get()->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0), glm::angleAxis(glm::radians(-40.f), glm::vec3(0, 0, 1)), glm::vec3(1)), TREBU_START);
        trebuchet_2.get()->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0), glm::angleAxis(glm::radians(40.f), glm::vec3(0, 0, 1)), glm::vec3(1)), TREBU_START+SPEEDZ);
        trebuchet_2.get()->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0), glm::angleAxis(glm::radians(40.f), glm::vec3(0, 0, 1)), glm::vec3(1)), TREBU_START+2.0);
        trebuchet_2.get()->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(0), glm::angleAxis(glm::radians(-40.f), glm::vec3(0, 0, 1)), glm::vec3(1)), TREBU_START+6.0);

        HierarchicalRenderable::addChild(trebuchet_1, trebuchet_2);
        viewer.addRenderable(trebuchet_1);

        // Submarine
        Submarine sub = createSubmarine(shaders[SHADER_FULL], viewer);
        sub.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3{-1, 0.2, 10}, glm::quat(glm::vec3{0, -PI_2, 0})), 0);
        glm::quat quatRot = glm::angleAxis(glm::radians(90.f), glm::vec3(0, 1, 0));
        glm::quat finalQuatRot = glm::angleAxis(glm::radians(180.f), glm::vec3(0, 1, 0));

        auto projectile = sub.container;
        auto basePos = glm::vec3(-5,-0.5,-2);
        projectile->addParentTransformKeyframe(GeometricTransformation(basePos, quatRot), 0);
        projectile->addParentTransformKeyframe(GeometricTransformation(basePos, quatRot), TREBU_START);
        projectile->addParentTransformKeyframe(GeometricTransformation(basePos+glm::vec3(-2, 5, 0), quatRot), TREBU_START+SPEEDZ);
        projectile->addParentTransformKeyframe(GeometricTransformation(basePos+glm::vec3(-200*2, 150*2, 0), quatRot), TREBU_START+SPEEDZ+4);
        viewer.addRenderable(projectile);

        // set camera traveling 
        GravitingAnimation* graviting = new GravitingAnimation(glm::vec3(0,0,0), 20, 3.2, 0, TREBU_START + 0.5f);
        graviting->setHeightModifier(0,-1.5);
        graviting->setHeightModifier(3,-2);
        graviting->setHeightModifier(4,5);
        graviting->setHeightModifier(6,0);
        viewer.getCamera().addAnimation(graviting);
        TravelingAnimation* traveling = new TravelingAnimation(glm::vec3(6.20166, 0, 19.0142), glm::vec3(-5,-2,12), glm::vec3(0,0,0), TREBU_START+1, SCENE_END - 0.5);
        viewer.getCamera().addAnimation(traveling);

        // Start the whole machine
        viewer.resetAnimation();
        viewer.startAnimation();

        // Stop scene after x seconds.
        enableCallback(viewer, 0, SCENE_END);
    }
};

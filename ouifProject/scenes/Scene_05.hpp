#pragma once

#include "Scene.hpp"
#include <Utils.hpp>
#include <FullRenderable.hpp>
#include <FrameRenderable.hpp>
#include <lighting/LightedMeshRenderable.hpp>
#include <lighting/Material.hpp>
#include <lighting/Light.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <texturing/TexturedCylinderRenderable.hpp>
#include <texturing/TexturedCubeRenderable.hpp>
#include <CameraAnimation.hpp>
#include <iostream>

#include "../Factory.hpp"

class Scene_05 : public Scene {
public:
    Scene_05(std::function<void(int)> callback) : Scene(callback) {}
    
    void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) {
        // Light source
        viewer.setDirectionalLight(std::make_shared<DirectionalLight>(
            /* Direction : */ glm::normalize(glm::vec3(0.0, -1.0, -2.0)),
            /* Ambiant :   */ glm::vec3(0.600, 0.500, 0.700),
            /* Diffuse :   */ glm::vec3(0.1,0.1,0.3),
            /* Specular :  */ glm::vec3(0.1,0.05,0.15)
        ));

        //ShaderProgramPtr sb = std::make_shared<ShaderProgram>("../../sfmlGraphicsPipeline/shaders/wtfVertex.glsl", "../../sfmlGraphicsPipeline/shaders/wtfFragment.glsl");
        //viewer.addShaderProgram(sb);

        // Submarine
        Submarine submarine = createSubmarine(shaders[SHADER_FULL], viewer);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(10,0,30), glm::quat(glm::vec3(0,glm::radians(-130.f),0))), 0);

        Fish fish = createFish(shaders[SHADER_FULL], viewer);
        fish.body->addParentTransformKeyframe(GeometricTransformation(glm::vec3(-10,0,30), glm::quat(glm::vec3(0,glm::radians(-130.f),0))), 0);

        auto fond = std::make_shared<TexturedPlaneRenderable>(shaders[SHADER_TEXT], getTexture("sunset.jpg"));
        glm::mat4 fond_transform(1);
        fond_transform = glm::translate(fond_transform, glm::vec3(0,25,50));
        fond_transform = glm::scale(fond_transform, glm::vec3(180));
        fond->setParentTransform(fond_transform);
        viewer.addRenderable(fond);

        // Caméra
        viewer.getCamera().setViewMatrix(glm::lookAt(glm::vec3(0,10,0), glm::vec3(0,0,30), glm::vec3(0,1,0)));
        
        // Finalization
        viewer.startAnimation();
    }
};

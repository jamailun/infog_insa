#pragma once

#include "Scene.hpp"
#include <CubeRenderable.hpp>
#include <FullRenderable.hpp>
#include <lighting/LightedMeshRenderable.hpp>
#include <lighting/Material.hpp>
#include <lighting/Light.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <lighting/DirectionalLightRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <CameraAnimation.hpp>
#include <iostream>

#include "../Factory.hpp"

class Scene_01 : public Scene {
public:
    Scene_01(std::function<void(int)> callback) : Scene(callback) {}
    
    void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) {
        float SCENE_END = 13.2;

        // Light source
        DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(
            /* Direction : */ glm::normalize(glm::vec3(0.0,-1.0,-1.0)),
            /* Ambiant :   */ glm::vec3(0.788, 0.886, 1.0),
            /* Diffuse :   */ glm::vec3(0.3,0.3,0.1),
            /* Specular :  */ glm::vec3(0.3,0.3,0.1)
        );
        viewer.setDirectionalLight(directionalLight);
        MaterialPtr textureMaterial = Material::TextureMaterial();

        // Submarine
        Submarine submarine = createSubmarine(shaders[SHADER_FULL], viewer);
        auto subTransform = submarine.main->getParentTransform();
        subTransform = glm::translate(subTransform, glm::vec3(0,50,0));
        submarine.main->setParentTransform(subTransform);

        submarine.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3(90,40,0)), 0);
        submarine.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,0.5,0), glm::angleAxis(glm::radians(-100.f), glm::vec3(.2, 1, 0))), 1.5);
        submarine.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,0.6,0), glm::angleAxis(glm::radians(150.f), glm::vec3(0, 1, 0))), 5.5);
        submarine.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,-0.6,-2), glm::angleAxis(glm::radians(150.f), glm::vec3(0, 1, 0))), 7.5);
        rotatePeriscope(submarine, 6.3, 1.6);
        rotatePeriscope(submarine, 9, 2);
        rotatePeriscope(submarine, 11.2, 2);
        submarine.main->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,-5,-5), glm::angleAxis(glm::radians(150.f), glm::vec3(0, 1, 0))), 12.5);

        // Ciel
        auto ciel = std::make_shared<TexturedCylinderRenderable>(shaders[SHADER_TEXT], getTexture("ciel.jpg"));
        glm::mat4 ciel_transform(1);
        ciel_transform = glm::translate(ciel_transform, glm::vec3(0,0,10));
        ciel_transform = glm::scale(ciel_transform, glm::vec3(50,90,50));
        ciel_transform = glm::rotate(ciel_transform, -1.57079f, glm::vec3(1,0,0));
        ciel->setParentTransform(ciel_transform);
        viewer.addRenderable(ciel);

        // Water
        auto watpl = std::make_shared<TexturedPlaneRenderable>(shaders[SHADER_WATR], getTexture("ciel.jpg"));
        glm::mat4 watpl_transform(1);
        watpl_transform = glm::translate(watpl_transform, glm::vec3(0,0,30));
        watpl_transform = glm::scale(watpl_transform, glm::vec3(140,1,120));
        watpl_transform = glm::rotate(watpl_transform, -1.57079f, glm::vec3(1,0,0));
        watpl->setParentTransform(watpl_transform);
        viewer.addRenderable(watpl);

        // fish
        //Fish fish = createFish(shaders[SHADER_FULL], viewer);
        //openMouth(fish, 0, 5.0, 20);
        //twerk(fish, 0, 5.0, 15);

        TravelingAnimation* traveling = new TravelingAnimation(glm::vec3(0,5,-10), glm::vec3(0,10,-25), glm::vec3(0,0,0), 0, SCENE_END - 0.5);
        viewer.getCamera().addAnimation(traveling);

        viewer.startAnimation();

        enableCallback(viewer, 1, SCENE_END);
    }
};

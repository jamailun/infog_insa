#pragma once

#include "Scene.hpp"
#include <Utils.hpp>
#include <FullRenderable.hpp>
#include <FrameRenderable.hpp>
#include <lighting/LightedMeshRenderable.hpp>
#include <lighting/Material.hpp>
#include <lighting/Light.hpp>
#include <lighting/PointLightRenderable.hpp>
#include <texturing/TexturedPlaneRenderable.hpp>
#include <texturing/TexturedCylinderRenderable.hpp>
#include <texturing/TexturedCubeRenderable.hpp>
#include <CameraAnimation.hpp>
#include <iostream>

#include "../Factory.hpp"

class Scene_04 : public Scene {
public:
    Scene_04(std::function<void(int)> callback) : Scene(callback) {}
    
    void loadAndStart(Viewer& viewer, ShaderProgramPtr shaders[]) {

        // Light source
        DirectionalLightPtr directionalLight = std::make_shared<DirectionalLight>(
            /* Direction : */ glm::normalize(glm::vec3(0.0, -1.0, -2.0)),
            /* Ambiant :   */ glm::vec3(0.600, 0.500, 0.700),
            /* Diffuse :   */ glm::vec3(0.1,0.1,0.3),
            /* Specular :  */ glm::vec3(0.1,0.05,0.15)
        );
        viewer.setDirectionalLight(directionalLight);
        MaterialPtr textureMaterial = Material::TextureMaterial();

        ParticleSystem ps = initParticleSystem(viewer, glm::vec3{0,5,0});

        // Arrière plan
        auto fond = std::make_shared<TexturedPlaneRenderable>(shaders[SHADER_TEXT], getTexture("underwater_1.jpg"));
        glm::mat4 fond_transform(1);
        fond_transform = glm::translate(fond_transform, glm::vec3(0,30,213));
        fond_transform = glm::scale(fond_transform, glm::vec3(180));
        fond->setParentTransform(fond_transform);
        viewer.addRenderable(fond);

        // Temple
        auto temple = std::make_shared<FullRenderable>(shaders[SHADER_FULL], getMesh("temple/temple.obj"), getTexture("temple/temple_color.png"));
        glm::mat4 temple_transform = temple->getModelMatrix();
        temple_transform = glm::scale(temple_transform, glm::vec3(5));
        temple->setParentTransform(temple_transform);
        viewer.addRenderable(temple);

        // Submarine
        Submarine submarine = createSubmarine(shaders[SHADER_FULL], viewer);

        // Submarine movements
        glm::vec3 sub_pos_1 = glm::vec3(0, -24, 100);
        glm::vec3 sub_pos_2 = glm::vec3(4, -24, 42);
        glm::vec3 sub_pos_3 = glm::vec3(4, -24, 20);
        glm::vec3 sub_pos_f = glm::vec3(-9, -24, 30);
        glm::mat4 orientation = glm::lookAt(sub_pos_2, sub_pos_1, glm::vec3(0,1,0));
        
        glm::vec3 delta_moving_fish = glm::vec3(0, 0, 10);
        
        submarine.container->addParentTransformKeyframe(GeometricTransformation(sub_pos_1, glm::quat(orientation)), 0);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(sub_pos_2, glm::quat(orientation)), 4);
        rotateFan(submarine, 0, 4, 1.8);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(sub_pos_3, glm::quat(orientation)), 6);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(sub_pos_3, glm::quat(orientation)), 23); // constant from 6 to 23
        submarine.container->addParentTransformKeyframe(GeometricTransformation(sub_pos_f, glm::quat(orientation)), 23.5);
        rotateFan(submarine, 4.1, 2, 0.9);

        // Fish
        glm::vec3 fish_pos = glm::vec3(0, -24, -33);
        glm::vec3 fish_scale = glm::vec3(2);
        Fish fish = createFish(shaders[SHADER_FULL], viewer);
        fish.body->addParentTransformKeyframe(GeometricTransformation(fish_pos, glm::quat(glm::vec3(0,-M_PI/2,0)), fish_scale), 0);
        fish.body->addParentTransformKeyframe(GeometricTransformation(fish_pos, glm::quat(glm::vec3(0,-M_PI/2,0)), fish_scale), 9);
        fish.body->addParentTransformKeyframe(GeometricTransformation(fish_pos, glm::quat(glm::vec3(0,M_PI/2,0)), fish_scale), 12);
        fish.body->addParentTransformKeyframe(GeometricTransformation(fish_pos, glm::quat(glm::vec3(0,M_PI/2,0)), fish_scale), 16); // constant from 12 to 16

        twerk(fish, 5, 7.5, 15);
        openMouth(fish, 13, 2.1, 20);
        fish.jaw->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.0f), glm::vec3(1, 0, 0))), 16);
        fish.jaw->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(0.0f), glm::vec3(1, 0, 0))), 18); // constant from 16 to 18
        
        // will soon launch missile.
        fish.body->addParentTransformKeyframe(GeometricTransformation(fish_pos + delta_moving_fish, glm::quat(glm::vec3(0,M_PI/2,0)), fish_scale * 1.5f), 18);
        fish.jaw->addLocalTransformKeyframe(GeometricTransformation(glm::vec3(), glm::angleAxis(glm::radians(20.0f), glm::vec3(0, 0, 1))), 20);

        float ralenti_start = 21;
        float ralenti_duration = 8;
        float submarine_hit = ralenti_start + ralenti_duration + 1;

        // Missile
        Missile missile = createMissile(shaders[SHADER_FULL], viewer);
        missile.body->addParentTransformKeyframe(GeometricTransformation(fish_pos, glm::quat(), glm::vec3(0.01)), 0);
        missile.body->addParentTransformKeyframe(GeometricTransformation(fish_pos + glm::vec3(0,1,0), glm::quat(), glm::vec3(0.01)), 20); // constant from 0 to 20
        missile.body->addParentTransformKeyframe(GeometricTransformation(fish_pos + glm::vec3(0,1,7), glm::quat(), glm::vec3(3)), ralenti_start);
        missile.body->addParentTransformKeyframe(GeometricTransformation(fish_pos + glm::vec3(0,1,30), glm::quat(), glm::vec3(3)), ralenti_start+ralenti_duration);
        missile.body->addParentTransformKeyframe(GeometricTransformation(sub_pos_f + glm::vec3(0,1,0), glm::quat(), glm::vec3(1)), submarine_hit);

        rotateMissileFan(missile, ralenti_start - 2, 2, 6);
        rotateMissileFan(missile, ralenti_start, ralenti_duration, 0.5);
        rotateMissileFan(missile, ralenti_start +ralenti_duration, 1, 6);
        
        // Caméra
        glm::vec3 cam_pos = glm::vec3(20,-24,1);
        glm::vec3 cam_pos_close_sub = cam_pos + glm::vec3(0, 0, 30);
        glm::vec3 cam_pos_close_fish = glm::vec3(6.95379, -24, -5);
        glm::vec3 cam_pos_see_missile = glm::vec3(-18.5, -24, -11);
        viewer.getCamera().setViewMatrix(glm::lookAt(cam_pos, sub_pos_2, glm::vec3(0,1,0)));

        
        // 1 : traveling introduisant le sous-marin
        TravelingAnimation* traveling_0 = new TravelingAnimation(sub_pos_3, cam_pos, sub_pos_2, 0, 3);
        viewer.getCamera().addAnimation(traveling_0);

        // 2 : une fois le sm arrivé, on dévoile le poisson
        RotationAnimation* rotation_0 = new RotationAnimation(cam_pos, sub_pos_3, fish_pos, 5, 8);
        viewer.getCamera().addAnimation(rotation_0);

        // 3 : on recule devant la menace du poisson
        TravelingAnimation* traveling_1 = new TravelingAnimation(cam_pos, cam_pos_close_sub, fish_pos, 12, 15);
        viewer.getCamera().addAnimation(traveling_1);

        // 4 : zoom dramatique
        TravelingAnimation* traveling_2 = new TravelingAnimation(cam_pos_close_sub, cam_pos_close_fish, fish_pos, 15.5, 16.2);
        viewer.getCamera().addAnimation(traveling_2);
        
        // 4 : zoom dramatique
        TravelingAnimation* traveling_3 = new TravelingAnimation(cam_pos_close_fish, cam_pos_close_fish+delta_moving_fish, fish_pos, 16.3, 18);
        viewer.getCamera().addAnimation(traveling_3);
        
        // 5 : décalage de l'autre côté pour voir le missile
        TravelingAnimation* traveling_4 = new TravelingAnimation(cam_pos_close_fish+delta_moving_fish, cam_pos_see_missile, fish_pos + glm::vec3(0,1,7), 20, 22);
        viewer.getCamera().addAnimation(traveling_4);
        
        // 6 : rotation pour observer l'avancement du missile
        RotationAnimation* rotation_1 = new RotationAnimation(cam_pos_see_missile, fish_pos + glm::vec3(0,1,7), fish_pos + glm::vec3(0,1,20), 22.01, 22.8);
        viewer.getCamera().addAnimation(rotation_1);
        
        // 7 : suivi, au ralenti, du missile
        TravelingRotationAnimation* traveling_5 = new TravelingRotationAnimation(
            // Positions
            cam_pos_see_missile,
            cam_pos_see_missile+ glm::vec3(0,1,25),
            // Looked points
            fish_pos + glm::vec3(0,1,20), 
            fish_pos + glm::vec3(0,1,40),
            // Durations
            23, submarine_hit - 0.5
        );
        viewer.getCamera().addAnimation(traveling_5);

        // 7 : suite de précédemment, vers le sous-marin, rapidement.
        RotationAnimation* rotation_2 = new RotationAnimation(
            // Position
            cam_pos_see_missile+ glm::vec3(0,1,25),
            // Looked points
            fish_pos + glm::vec3(0,1,40),
            sub_pos_f,
            // Durations
            submarine_hit - 0.5,
            submarine_hit + 0.2
        );
        viewer.getCamera().addAnimation(rotation_2);
        
        // end
        submarine.container->addParentTransformKeyframe(GeometricTransformation(sub_pos_f, glm::quat(orientation)), submarine_hit-0.0001);
        submarine.container->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,50,0), glm::quat(), glm::vec3(0.0001)), submarine_hit+0.0001);
        missile.body->addParentTransformKeyframe(GeometricTransformation(glm::vec3(0,50,0), glm::quat(), glm::vec3(0.0001)), submarine_hit+0.0001);

        // Finalization
        viewer.startAnimation();
    }
};

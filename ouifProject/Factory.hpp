#pragma once

#include <FullRenderable.hpp>
#include <Renderable.hpp>
#include <Viewer.hpp>
#include <ShaderProgram.hpp>

#include <dynamics/DynamicSystemRenderable.hpp>
#include <dynamics/RealConstantForceField.hpp>
#include <dynamics/Particle.hpp>
#include <dynamics/ParticleRenderable.hpp>
#include <dynamics/EulerExplicitSolver.hpp>

constexpr float PI_2 = 1.5708f;

inline std::string getMesh(std::string mesh);
inline std::string getTexture(std::string texture);
inline std::string getShader(std::string shader);

struct Submarine {
    ContainerRenderablePtr container;
    FullRenderablePtr main;
    FullRenderablePtr periscope;
    FullRenderablePtr fan;
};

struct Fish {
    ContainerRenderablePtr container;
    FullRenderablePtr body;
    FullRenderablePtr jaw;
    FullRenderablePtr lantern;
    FullRenderablePtr assL;
    FullRenderablePtr assR;
};

struct Missile {
    FullRenderablePtr body;
    FullRenderablePtr fan;
    glm::vec3 decalage_fan;
    glm::vec3 scale_fan;
};

struct ParticleSystem {
    DynamicSystemPtr system;
    DynamicSystemRenderablePtr renderer;
};

// Submarine
void rotateFan(Submarine& sub, float from, float duration, float speed);
void rotatePeriscope(Submarine& sub, float from, float duration);
Submarine createSubmarine(ShaderProgramPtr& shader, Viewer& viewer);

// Fish
void twerk(Fish& fish, float from, float duration, int amount);
void openMouth(Fish& fish, float from, float duration, int amount);
Fish createFish(ShaderProgramPtr& shader, Viewer& viewer);

// particles
ParticleSystem initParticleSystem(Viewer& viewer, glm::vec3 constantForce);
inline ParticleSystem initParticleSystem(Viewer& viewer) {
    return initParticleSystem(viewer, glm::vec3(0));
}
void createParticle(ShaderProgramPtr& shader, ParticleSystem& system, glm::vec3 position, glm::vec3 velocity, float r, float m);

// Algues
FullRenderablePtr createAlgue(ShaderProgramPtr& shader, Viewer& viewer, glm::vec3 position);

// Missile
void rotateMissileFan(Missile& sub, float from, float duration, float speed);
Missile createMissile(ShaderProgramPtr& shader, Viewer& viewer);
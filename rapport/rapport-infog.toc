\babel@toc {french}{}\relax 
\contentsline {section}{\numberline {1}Nouveaux éléments de code}{3}{section.1}%
\contentsline {subsection}{\numberline {1.1}Classe FullRenderable}{3}{subsection.1.1}%
\contentsline {subsection}{\numberline {1.2}Système de scène}{3}{subsection.1.2}%
\contentsline {subsection}{\numberline {1.3}Factory}{4}{subsection.1.3}%
\contentsline {subsection}{\numberline {1.4}Animations de caméra}{5}{subsection.1.4}%
\contentsline {subsection}{\numberline {1.5}Classe TexturedCylinderRenderable}{6}{subsection.1.5}%
\contentsline {section}{\numberline {2}Modifications structurales du code déjà existant}{7}{section.2}%
\contentsline {subsection}{\numberline {2.1}Système de callback}{7}{subsection.2.1}%
\contentsline {subsection}{\numberline {2.2}Problèmes liés au système de CallBack}{9}{subsection.2.2}%
\contentsline {section}{\numberline {3}Shaders}{10}{section.3}%
\contentsline {subsection}{\numberline {3.1}Bulles}{10}{subsection.3.1}%
\contentsline {subsection}{\numberline {3.2}Eau}{10}{subsection.3.2}%
\contentsline {section}{\numberline {4}Modèles}{12}{section.4}%
\contentsline {subsection}{\numberline {4.1}Le sous-marin}{12}{subsection.4.1}%
\contentsline {subsection}{\numberline {4.2}Le poisson}{13}{subsection.4.2}%
\contentsline {subsection}{\numberline {4.3}Le temple}{14}{subsection.4.3}%
\contentsline {section}{\numberline {5}Manuel utilisateur}{15}{section.5}%
\contentsline {section}{\numberline {6}Notes additionnelles}{16}{section.6}%

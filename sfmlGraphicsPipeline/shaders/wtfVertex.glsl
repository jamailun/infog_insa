uniform mat4 projMat, viewMat, modelMat;
uniform float time;

in vec3 vNormal;
in vec3 vPosition;
in vec2 vTexCoord;
out vec2 surfel_texCoord;

/* Stolen from https://stackoverflow.com/questions/4200224/random-noise-functions-for-glsl */

// A single iteration of Bob Jenkins' One-At-A-Time hashing algorithm.
uint hash( uint x ) {
    x += ( x << 10u );
    x ^= ( x >>  6u );
    x += ( x <<  3u );
    x ^= ( x >> 11u );
    x += ( x << 15u );
    return x;
}
// Compound versions of the hashing algorithm I whipped together.
uint hash( uvec2 v ) { return hash( v.x ^ hash(v.y)                         ); }
uint hash( uvec3 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z)             ); }
uint hash( uvec4 v ) { return hash( v.x ^ hash(v.y) ^ hash(v.z) ^ hash(v.w) ); }
// Construct a float with half-open range [0:1] using low 23 bits.
// All zeroes yields 0.0, all ones yields the next smallest representable value below 1.0.
float floatConstruct( uint m ) {
    const uint ieeeMantissa = 0x007FFFFFu; // binary32 mantissa bitmask
    const uint ieeeOne      = 0x3F800000u; // 1.0 in IEEE binary32
    m &= ieeeMantissa;                     // Keep only mantissa bits (fractional part)
    m |= ieeeOne;                          // Add fractional part to 1.0
    float  f = uintBitsToFloat( m );       // Range [1:2]
    return f - 1.0;                        // Range [0:1]
}

// Pseudo-random value in half-open range [0:1].
float random( float x ) { return floatConstruct(hash(floatBitsToUint(x))); }
float random( vec2  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec3  v ) { return floatConstruct(hash(floatBitsToUint(v))); }
float random( vec4  v ) { return floatConstruct(hash(floatBitsToUint(v))); }

/* End of the theft */

void main() {
    float force = random(vPosition);
    vec3 direction = vec3(vPosition);
    vec3 np = vPosition + (direction*time*force);

    gl_Position = projMat*viewMat*modelMat*vec4(np, 1.0f);
    surfel_texCoord = vTexCoord;
}

/*
Juste le faire trembler en mode épilepsie
gl_Position = projMat*viewMat*modelMat*vec4(vPosition, 1.0f);
    float force = random(vec4(gl_Position.xyz, time)); // random from position and time
    vec4 dir = vec4(vNormal * force, 1.0f);
    gl_Position += dir;
    surfel_texCoord = vTexCoord;
*/
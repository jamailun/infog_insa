#version 400

//Design choice : Color are always vec4
//uniform mat4 camMat;
//uniform vec3 camPos;
uniform vec3 camForward;

in vec4 fragmentColor;

out vec4 outColor;

// RGB vec3 to vec4 between 0 and 1
vec4 col(int r, int g, int b) {
    return vec4(r,g,b,255.0) / 255.0;
}

vec4 style1(float val) {
    if(val > 0.10)
        return col(141, 137, 203);
    if(val > 0.06)
        return col(101, 93, 205);
    if(val > 0.03)
        return col(63, 52, 215);
    return col(35, 22, 216);
}
vec4 style2(float val) {
    if(val > 0.10)
        return col(200, 200, 230);
    if(val > 0.06)
        return col(220, 220, 250);
    if(val > 0.03)
        return col(254, 254, 254);
    return col(255, 255, 255);
}

void main() {
    float dotProdA = abs( dot(normalize (fragmentColor.xyz), normalize(camForward)) );

    if(dotProdA > 0.12)
        discard;
    
    outColor = style2(dotProdA);
}

uniform mat4 modelViewProjection;
uniform mat4 modelMatrix;
uniform mat4 modelInverse; //inverse of the model matrix
uniform vec4 eye; // -- position of camera (x,y,z,1)
//uniform vec4 light; //--directional light direction (x,y,z,0)
uniform float fogRadius;
uniform vec4 lightColor; //--directional light colour
uniform float time;// animate explosion
//uniform bool hasTexture;
const vec4 grav = vec4(0.,0.,-0.02,0.);
const float friction = 0.02;

attribute vec4 position;
attribute vec4 color;
attribute vec2 texCoord;
attribute vec3 normal;
attribute vec4 origin; //centre of each face
attribute vec4 trajectory; // trajectory + w = angular velocity

varying lowp vec4 vColor;
varying float dist;
varying highp vec2 vTexCoord;
varying vec4 vNormal;

void main()
{
    float angle = time * trajectory.w;
    float angCos = cos(angle);
    float angSin = sin(angle);
    lowp mat2 rotMat = mat2(angCos, angSin, -angSin, angCos); 
    vec3 normRot = normal;
    normRot.xy = rotMat * normRot.xy;

    vNormal = normalize(modelMatrix * vec4( normRot, 0.0 ));
   // vDirectDiffuse = lightColor * max( 0.0, dot( norm, light )); // brightness of diffuse light

    vec4 gravity = modelInverse * grav; //convert world gravity vector into model coords
    highp vec4 A = gravity/(friction*friction) - vec4(trajectory.xyz, 0.)/friction;
    highp vec4 B = origin - A;

    vec4 pos = position - origin; // convert to local
    pos.xy = rotMat * pos.xy; // rotate
    pos += exp(-time*friction)*A + B + time * gravity/friction;

    vec4 vPosition = modelMatrix * pos;

    dist = clamp(1.0-distance(vPosition.xyz, eye.xyz)/fogRadius+0.1, 0.0, 1.1); //(vPosition.y-eye.y)

    vColor = color;
    vTexCoord = texCoord;
    gl_Position = modelViewProjection * pos;
}
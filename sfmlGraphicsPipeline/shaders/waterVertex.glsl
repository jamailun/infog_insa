#version 400

uniform mat4 projMat, viewMat, modelMat;
in vec3 vPosition;
in vec4 vColor;
in vec2 vTexCoord;

out vec2 surfel_texCoord;
out vec4 fragmentColor;

void main() {
    gl_Position = projMat*viewMat*modelMat*vec4(vPosition, 1.0f);

    fragmentColor = vec4(vPosition, 1.0);
    surfel_texCoord = vTexCoord;
}

#version 400

//Design choice : Color are always vec4
uniform vec3 camForward;
uniform vec2 vTexCoord;
uniform sampler2D texSampler;
uniform float time;

in vec2 surfel_texCoord;
in vec4 fragmentColor;

out vec4 outColor;

// RGB vec3 to vec4 between 0 and 1
vec4 col(int r, int g, int b) {
    return vec4(r,g,b,255.0) / 255.0;
}

void main() {
    vec2 resolution = vec2(1.,1.)*800;
    //vec3 col = vec3(0.5+0.5*fragmentColor.xyz);
    //outColor = vec4(col.x, col.y, col.z, 1.0f);

    vec2 cPos = (5.0 * gl_FragCoord.xy / resolution.xy) - 5.0;
    float cLength = length(cPos);

    vec2 uv = gl_FragCoord.xy/resolution.xy + (cPos/cLength) * cos(cLength*12.0-time*2.0) * 0.03;
    vec3 col = texture2D(texSampler, uv*surfel_texCoord).xyz;

    outColor = vec4(col,1.0);

}

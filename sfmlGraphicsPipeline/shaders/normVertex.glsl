#version 400

uniform mat4 projMat, viewMat, modelMat;

in vec3 vPosition;

//Design choice : Color are always vec4
in vec4 vColor;
in vec3 vNormal;
out vec4 fragmentColor;

void main()
{
    gl_Position = projMat*viewMat*modelMat*vec4(vPosition, 1.0f);
    fragmentColor = vec4(0.5+0.5*vNormal, 1.0f);
}

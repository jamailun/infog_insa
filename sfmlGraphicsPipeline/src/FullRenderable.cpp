#include "./../include/texturing/TexturedLightedMeshRenderable.hpp"
#include "./../include/gl_helper.hpp"
#include "./../include/log.hpp"
#include "./../include/Io.hpp"
#include "./../include/Utils.hpp"
#include "./../include/FullRenderable.hpp"
#include "./../include/Viewer.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>
#include <iostream>

FullRenderable::~FullRenderable() {
    glcheck(glDeleteBuffers(1, &m_pBuffer));
    glcheck(glDeleteBuffers(1, &m_cBuffer));
    glcheck(glDeleteBuffers(1, &m_nBuffer));
    glcheck(glDeleteBuffers(1, &m_iBuffer));
    glcheck(glDeleteBuffers(1, &m_tBuffer));
    glcheck(glDeleteTextures(1, &m_texId));
}

FullRenderable::FullRenderable(ShaderProgramPtr shaderProgram, const std::string& mesh_filename, const std::string& texture_filename = "" ) :
    HierarchicalRenderable(shaderProgram),
    m_pBuffer(0), m_cBuffer(0), m_nBuffer(0), m_iBuffer(0), m_tBuffer(0), m_texId(0)
{
    read_obj(mesh_filename, m_positions, m_indices, m_normals, m_texCoords);
    m_colors.resize( m_positions.size(), glm::vec4(1.0,1.0,1.0,1.0) );

    //Create buffers
    glcheck(glGenBuffers(1, &m_pBuffer)); //vertices
    glcheck(glGenBuffers(1, &m_cBuffer)); //colors
    glcheck(glGenBuffers(1, &m_nBuffer)); //normals
    glcheck(glGenBuffers(1, &m_iBuffer)); //indices
    glcheck(glGenBuffers(1, &m_tBuffer)); //texture coordinates

    //Activate buffer and send data to the graphics card
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_positions.size()*sizeof(glm::vec3), m_positions.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_colors.size()*sizeof(glm::vec4), m_colors.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_normals.size()*sizeof(glm::vec3), m_normals.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_tBuffer));
    glcheck(glBufferData(GL_ARRAY_BUFFER, m_texCoords.size()*sizeof(glm::vec2), m_texCoords.data(), GL_STATIC_DRAW));
    glcheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBuffer));
    glcheck(glBufferData(GL_ELEMENT_ARRAY_BUFFER, m_indices.size()*sizeof(unsigned int), m_indices.data(), GL_STATIC_DRAW));

    // create and setup the texture
    glcheck(glGenTextures(1, &m_texId));
    glcheck(glBindTexture(GL_TEXTURE_2D, m_texId));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE));
    glcheck(glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE));
    //glActiveTexture(GL_TEXTURE1);
    //glGenerateMipmap(GL_TEXTURE_2D);

    // load the texture
    if(texture_filename != "") {
        sf::Image image;
        image.loadFromFile(texture_filename);
        image.flipVertically(); // sfml inverts the v axis... put the image in OpenGL convention: lower left corner is (0,0)
        glcheck(glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, image.getSize().x, image.getSize().y, 0, GL_RGBA, GL_UNSIGNED_BYTE, (const GLvoid*)image.getPixelsPtr()));

        //Release the texture
        glcheck(glBindTexture(GL_TEXTURE_2D, 0));
        textured = true;
    } else {
        textured = false;
    }
}

void FullRenderable::do_draw() {
    /*openGL_all(
        m_shaderProgram, m_camera, m_material, textured, getModelMatrix(),
        m_indices,
        m_pBuffer, m_cBuffer, m_nBuffer,
        m_iBuffer, m_tBuffer, m_texId
    );*/
    
    int positionLocation = m_shaderProgram->getAttributeLocation("vPosition");
    int colorLocation = m_shaderProgram->getAttributeLocation("vColor");
    int normalLocation = m_shaderProgram->getAttributeLocation("vNormal");
    int modelLocation = m_shaderProgram->getUniformLocation("modelMat");
    int nitLocation = m_shaderProgram->getUniformLocation("NIT");

    int timeLoc = m_shaderProgram->getUniformLocation("time");
    
    int camMatLocation = m_viewer != nullptr ? m_shaderProgram->getUniformLocation("camMat"): ShaderProgram::null_location;
    int camVecPos = m_viewer != nullptr ? m_shaderProgram->getUniformLocation("camPos"): ShaderProgram::null_location;
    int camVecForward = m_viewer != nullptr ? m_shaderProgram->getUniformLocation("camForward"): ShaderProgram::null_location;
    
    int texcoordLocation = textured ? m_shaderProgram->getAttributeLocation("vTexCoord") : ShaderProgram::null_location;
    int texsamplerLocation = textured ? m_shaderProgram->getUniformLocation("texSampler") : ShaderProgram::null_location;
    
    //Send material uniform to GPU
    Material::sendToGPU(m_shaderProgram, m_material);

    if(camMatLocation != ShaderProgram::null_location) {
        glcheck(glUniformMatrix4fv(camMatLocation, 1, GL_FALSE, glm::value_ptr(m_viewer->getCamera().projectionMatrix())));
    }
    if(camVecPos != ShaderProgram::null_location) {
        glcheck(glUniform3fv(camVecPos, 1, glm::value_ptr(m_viewer->getCamera().getPosition())));
    }
    if(camVecForward != ShaderProgram::null_location) {
        glcheck(glUniform3fv(camVecForward, 1, glm::value_ptr(m_viewer->getCamera().getForward())));
    }
    if(timeLoc != ShaderProgram::null_location) {
        glcheck(glUniform1f(timeLoc, m_viewer->getTime()));
    }
    if(modelLocation != ShaderProgram::null_location) {
        glcheck(glUniformMatrix4fv(modelLocation, 1, GL_FALSE, glm::value_ptr(getModelMatrix())));
    }
    if(positionLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(positionLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
        glcheck(glVertexAttribPointer(positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
    if(colorLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(colorLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
        glcheck(glVertexAttribPointer(colorLocation, 4, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
    if(normalLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(normalLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
        glcheck(glVertexAttribPointer(normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
    if(nitLocation != ShaderProgram::null_location ) {
        glcheck(glUniformMatrix3fv(nitLocation, 1, GL_FALSE,
          glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(getModelMatrix()))))));
    }
    if(texcoordLocation != ShaderProgram::null_location) {
        glcheck(glActiveTexture(GL_TEXTURE0));
        glcheck(glBindTexture(GL_TEXTURE_2D, m_texId));
        glcheck(glUniform1i(texsamplerLocation, 0));
        glcheck(glEnableVertexAttribArray(texcoordLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_tBuffer));
        glcheck(glVertexAttribPointer(texcoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }

    //draw
    glcheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBuffer));
    glcheck(glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, (void*)0));

    if(camMatLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(camMatLocation));
    }
    if(camVecPos != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(camVecPos));
    }
    if(camVecForward != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(camVecForward));
    }
    if(timeLoc != ShaderProgram::null_location){
        glcheck(glDisableVertexAttribArray(timeLoc));
    }
    if(positionLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(positionLocation));
    }
    if(colorLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(colorLocation));
    }
    if(normalLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(normalLocation));
    }
    if(texcoordLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(texcoordLocation));
    }
}

void FullRenderable::do_animate(float time) {
    if(!m_localKeyframes.empty()) {
        setLocalTransform( m_localKeyframes.interpolateTransformation( time ) );
    }
    if(!m_parentKeyframes.empty()) {
        setParentTransform( m_parentKeyframes.interpolateTransformation( time ) );
    }
}

void FullRenderable::setMaterial(const MaterialPtr& material) {
    m_material = material;
}

void FullRenderable::addLocalTransformKeyframe( const GeometricTransformation& transformation, float time ) {
  m_localKeyframes.add( transformation, time );
}

void FullRenderable::addParentTransformKeyframe( const GeometricTransformation& transformation, float time ) {
  m_parentKeyframes.add( transformation, time );
}

#include "../include/Keyframed.hpp"
#include "../include/gl_helper.hpp"
#include "../include/GeometricTransformation.hpp"
#include "./../include/Io.hpp"
#include "./../include/log.hpp"
#include "../include/Utils.hpp"

#include <glm/gtc/type_ptr.hpp>
#include <GL/glew.h>

void Keyframed::addLocalTransformKeyframe( const GeometricTransformation& transformation, float time ) {
  m_localKeyframes.add( transformation, time );
}

void Keyframed::addParentTransformKeyframe( const GeometricTransformation& transformation, float time ) {
  m_parentKeyframes.add( transformation, time );
}

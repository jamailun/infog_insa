#include "./../../include/dynamics/RealConstantForceField.hpp"

RealConstantForceField::RealConstantForceField(const glm::vec3& force) {
    m_force = force;
}

void RealConstantForceField::apply(ParticlePtr particle) {
    particle->incrForce(m_force*particle->getMass());
}

const glm::vec3& RealConstantForceField::getForce() {
    return m_force;
}

void RealConstantForceField::setForce(const glm::vec3& force) {
    m_force = force;
}

#include "./../include/GLUtils.hpp"
#include "./../include/Viewer.hpp"

void load_GL_basic(
    OpenGL_locations* locs, ShaderProgramPtr shader, Viewer* viewer, glm::mat4 modelMatrix,
    unsigned int m_pBuffer, unsigned int m_cBuffer, unsigned int m_nBuffer
) {
    locs->positionLocation = shader->getAttributeLocation("vPosition");
    locs->colorLocation = shader->getAttributeLocation("vColor");
    locs->normalLocation = shader->getAttributeLocation("vNormal");
    locs->modelLocation = shader->getUniformLocation("modelMat");
    locs->nitLocation = shader->getUniformLocation("NIT");

    locs->timeLoc = shader->getUniformLocation("time");
    
    locs->camMatLocation = viewer != nullptr ? shader->getUniformLocation("camMat"): ShaderProgram::null_location;
    locs->camVecPos = viewer != nullptr ? shader->getUniformLocation("camPos"): ShaderProgram::null_location;
    locs->camVecForward = viewer != nullptr ? shader->getUniformLocation("camForward"): ShaderProgram::null_location;

    if(locs->camMatLocation != ShaderProgram::null_location) {
        glcheck(glUniformMatrix4fv(locs->camMatLocation, 1, GL_FALSE, glm::value_ptr(viewer->getCamera().projectionMatrix())));
    }
    if(locs->camVecPos != ShaderProgram::null_location) {
        glcheck(glUniform3fv(locs->camVecPos, 1, glm::value_ptr(viewer->getCamera().getPosition())));
    }
    if(locs->camVecForward != ShaderProgram::null_location) {
        glcheck(glUniform3fv(locs->camVecForward, 1, glm::value_ptr(viewer->getCamera().getForward())));
    }
    if(locs->timeLoc != ShaderProgram::null_location) {
        glcheck(glUniform1f(locs->timeLoc, viewer->getTime()));
    }
    if(locs->modelLocation != ShaderProgram::null_location) {
        glcheck(glUniformMatrix4fv(locs->modelLocation, 1, GL_FALSE, glm::value_ptr(modelMatrix)));
    }
    if(locs->positionLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(locs->positionLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_pBuffer));
        glcheck(glVertexAttribPointer(locs->positionLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
    if(locs->colorLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(locs->colorLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_cBuffer));
        glcheck(glVertexAttribPointer(locs->colorLocation, 4, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
    if(locs->normalLocation != ShaderProgram::null_location) {
        glcheck(glEnableVertexAttribArray(locs->normalLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_nBuffer));
        glcheck(glVertexAttribPointer(locs->normalLocation, 3, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
    if(locs->nitLocation != ShaderProgram::null_location ) {
        glcheck(glUniformMatrix3fv(locs->nitLocation, 1, GL_FALSE,
          glm::value_ptr(glm::transpose(glm::inverse(glm::mat3(modelMatrix))))));
    }
}

void unload_GL_all(
    OpenGL_locations* locs
) {
    if(locs->camMatLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->camMatLocation));
    }
    if(locs->camVecPos != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->camVecPos));
    }
    if(locs->camVecForward != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->camVecForward));
    }
    if(locs->timeLoc != ShaderProgram::null_location){
        glcheck(glDisableVertexAttribArray(locs->timeLoc));
    }
    if(locs->positionLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->positionLocation));
    }
    if(locs->colorLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->colorLocation));
    }
    if(locs->normalLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->normalLocation));
    }
    if(locs->texcoordLocation != ShaderProgram::null_location) {
        glcheck(glDisableVertexAttribArray(locs->texcoordLocation));
    }
}

void load_GL_texture(
    OpenGL_locations* locs, ShaderProgramPtr shader,
    unsigned int m_tBuffer,unsigned int m_texId
) {
    locs->texcoordLocation = shader->getAttributeLocation("vTexCoord");
    locs->texsamplerLocation = shader->getUniformLocation("texSampler");

    if(locs->texcoordLocation != ShaderProgram::null_location) {
        glcheck(glActiveTexture(GL_TEXTURE0));
        glcheck(glBindTexture(GL_TEXTURE_2D, m_texId));
        glcheck(glUniform1i(locs->texsamplerLocation, 0));
        glcheck(glEnableVertexAttribArray(locs->texcoordLocation));
        glcheck(glBindBuffer(GL_ARRAY_BUFFER, m_tBuffer));
        glcheck(glVertexAttribPointer(locs->texcoordLocation, 2, GL_FLOAT, GL_FALSE, 0, (void*)0));
    }
}

void openGL_basic(
    ShaderProgramPtr shader,  Viewer* viewer, glm::mat4 modelMatrix,
    std::vector< unsigned int > m_indices, unsigned int m_iBuffer,
    unsigned int m_pBuffer, unsigned int m_cBuffer, unsigned int m_nBuffer
) {
    OpenGL_locations locs;
    load_GL_basic(&locs, shader, viewer, modelMatrix, m_pBuffer, m_cBuffer, m_nBuffer);

    glcheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBuffer));
    glcheck(glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, (void*)0));

    unload_GL_all(&locs);
}

void openGL_all(
    ShaderProgramPtr shader, Viewer* viewer, MaterialPtr material, bool textured, glm::mat4 modelMatrix,
    std::vector< unsigned int > m_indices,
    unsigned int m_pBuffer, unsigned int m_cBuffer, unsigned int m_nBuffer,
    unsigned int m_iBuffer, unsigned int m_tBuffer, unsigned int m_texId
) {
    OpenGL_locations locs;

    load_GL_basic(&locs, shader, viewer, modelMatrix, m_pBuffer, m_cBuffer, m_nBuffer);
    Material::sendToGPU(shader, material);
    
    if(textured)
        load_GL_texture(&locs, shader, m_tBuffer, m_texId);

    glcheck(glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_iBuffer));
    glcheck(glDrawElements(GL_TRIANGLES, m_indices.size(), GL_UNSIGNED_INT, (void*)0));

    unload_GL_all(&locs);
}



#pragma once

#include "./Camera.hpp"
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/type_precision.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <GL/glew.h>
#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>

class Camera;

// Classe mère, abstraite
class CameraAnimation {
    public:
        virtual glm::mat4 animate(Camera* camera, float time) = 0;
};

// Vecteur souvent utilisé dans les éléments suivants, indiquant le haut de la scène.
const glm::vec3 VIEW_UP( 0, 1, 0 );

// Traveling d'un point A à B, en regardant le même point
class TravelingAnimation : public CameraAnimation {
    public:
        TravelingAnimation(glm::vec3 from, glm::vec3 to, glm::vec3 lookedPoint, float begin, float end) :
            m_from(from), m_to(to), m_lookedPoint(lookedPoint), m_begin(begin), m_end(end)
        {
            float distance = glm::length(to - from);
            m_speed = distance/(end-begin);
            m_direction = glm::normalize(to - from);
        }

        glm::mat4 animate(Camera* camera, float time) {
            if(time < m_begin || time > m_end)
                throw "Out of timezone";
            glm::vec3 eye = m_from + ((time-m_begin) * m_direction * m_speed);
            return glm::lookAt(eye, m_lookedPoint, VIEW_UP);
        }
    private:
        glm::vec3 m_direction;
        glm::vec3 m_from;
        glm::vec3 m_to;
        glm::vec3 m_lookedPoint;
        float m_begin;
        float m_end;
        float m_speed;
};

// Rotation de la direction de la caméra, d'un point C à D
class RotationAnimation : public CameraAnimation {
    public:
        RotationAnimation(glm::vec3 position, glm::vec3 lookedFrom, glm::vec3 lookedTo, float begin, float end) :
            m_position(position), m_lookedFrom(lookedFrom), m_lookedTo(lookedTo), m_begin(begin), m_end(end)
        {
            m_speed = glm::length(lookedTo - lookedFrom)/(end-begin);
            m_direction = glm::normalize(lookedTo - lookedFrom);
        }

        glm::mat4 animate(Camera* camera, float time) {
            if(time < m_begin || time > m_end)
                throw "Out of timezone";
            glm::vec3 lookedPosition = m_lookedFrom + ((time-m_begin) * m_direction * m_speed);
            return glm::lookAt(m_position, lookedPosition, VIEW_UP);
        }
    private:
        glm::vec3 m_direction;
        glm::vec3 m_position;
        glm::vec3 m_lookedTo;
        glm::vec3 m_lookedFrom;
        float m_begin;
        float m_end;
        float m_speed;
};

// Traveling d'un point A à B, avec une rotation de la caméra d'un point C à D
class TravelingRotationAnimation : public CameraAnimation {
    public:
        TravelingRotationAnimation(glm::vec3 from, glm::vec3 to, glm::vec3 lookedPointFrom, glm::vec3 lookedPointTo, float begin, float end) :
            m_from(from), m_to(to), m_lookedPointFrom(lookedPointFrom), m_lookedPointTo(lookedPointTo), m_begin(begin), m_end(end)
        {
            m_speed_pos = glm::length(to - from)/(end-begin);
            m_direction_pos = glm::normalize(to - from);
            m_speed_look = glm::length(lookedPointTo - lookedPointFrom)/(end-begin);
            m_direction_look = glm::normalize(lookedPointTo - lookedPointFrom);
        }

        glm::mat4 animate(Camera* camera, float time) {
            if(time < m_begin || time > m_end)
                throw "Out of timezone";
            glm::vec3 eye = m_from + ((time-m_begin) * m_direction_pos * m_speed_pos);
            glm::vec3 look = m_lookedPointFrom + ((time-m_begin) * m_direction_look * m_speed_look);
            return glm::lookAt(eye, look, VIEW_UP);
        }
    private:
        glm::vec3 m_direction_pos;
        glm::vec3 m_direction_look;
        glm::vec3 m_from;
        glm::vec3 m_to;
        glm::vec3 m_lookedPointFrom;
        glm::vec3 m_lookedPointTo;
        float m_begin;
        float m_end;
        float m_speed_pos;
        float m_speed_look;
};

// Traveling de la caméra d'un point A à B, mais en regardant dans une direction spécifique
class MonoTravelingAnimation : public CameraAnimation {
    public:
        MonoTravelingAnimation(glm::vec3 from, glm::vec3 to, glm::vec3 lookedDirection, float begin, float end) :
            m_from(from), m_to(to), m_lookedDirection(lookedDirection), m_begin(begin), m_end(end)
        {
            float distance = glm::length(to - from);
            m_speed = distance/(end-begin);
            m_direction = glm::normalize(to - from);
        }

        glm::mat4 animate(Camera* camera, float time) {
            if(time < m_begin || time > m_end)
                throw "Out of timezone";
            glm::vec3 eye = m_from + ((time-m_begin) * m_direction * m_speed);
            glm::vec3 looked = eye + m_lookedDirection;
            return glm::lookAt(eye, looked, VIEW_UP);
        }
    private:
        glm::vec3 m_direction;
        glm::vec3 m_from;
        glm::vec3 m_to;
        glm::vec3 m_lookedDirection;
        float m_begin;
        float m_end;
        float m_speed;
};

// ~Keyframe, mais avec une simple valeur de "hauteur" et non pas une matrice
struct HeightPair {
    float timing;
    float value;
};
bool comp_HeightPair(HeightPair a, HeightPair b);

// Rotation autour d'un point, à une certaine distance, et avec des transitions optionnelles de hauteur.
class GravitingAnimation : public CameraAnimation {
    public:
        GravitingAnimation(glm::vec3 center, float distance, float turnsAmount, float begin, float end) :
            m_center(center), m_distance(distance), m_begin(begin), m_end(end), m_height(0)
        {
            m_radPerSec = glm::radians(360.0f*turnsAmount)/(end-begin);
        }

        glm::mat4 animate(Camera* camera, float time) {
            if(time < m_begin || time > m_end)
                throw "Out of timezone";
            // calculate height
            if( ! heights.empty()) {
                calculate_height(time);
            }
            // calculate angle and position
            float radAngle = (time-m_begin) * m_radPerSec;
            glm::vec3 eye = calculate_circle_position(radAngle);
            // return the result
            return glm::lookAt(eye, m_center, VIEW_UP);
        }

        void setHeightModifier(float time, float height) {
            HeightPair pair = {time, height};
           // std::cout << "added {" << pair.timing << ";" << pair.value << "}.\n";
            heights.push_back(pair);
            if(heights.size() > 1) {
                std::sort(heights.begin(), heights.end(), comp_HeightPair);
            }
           // std::cout << "on a désormais " << heights.size() << " heights.\n";
        }
    private:
        glm::vec3 calculate_circle_position(float rad) {
            return (m_distance * glm::vec3(std::cos(rad), 0, std::sin(rad))) + (m_height * VIEW_UP);
        }
        void calculate_height(float time) {
            if(heights.size() == 1) {
                m_height = heights.begin()->value;
                return;
            }
            HeightPair a, b;
            bool al = false;
            bool bl = false;
            for(std::vector<HeightPair>::iterator it = heights.begin(); it != heights.end(); ++it) {
                if((*it).timing <= time) {
                    a = (*it);
                    al = true;
                } else {
                    b = (*it);
                    bl = true;
                    break;
                }
            }
            if(!al) {
                if(!bl) {
                    m_height = 0;
                    std::cout << "ça alors, pas trouvé de hauteur !\n";
                    return;
                }
                std::cout << "["<<time<<"] pas de A !\n";
                m_height = b.value;
                return;
            }
            if(!bl) {
                m_height = a.value;
               // std::cout << "["<<time<<"] pas de B !\n";
                return;
            }
            float epsilon = (time - a.timing)/(b.timing - a.timing);
            float delta = std::abs(b.value - a.value);
            m_height = b.value + ((1-epsilon)*delta);
            //std::cout << "A= {" << a.timing << ";" << a.value << "}.\n";
            //std::cout << "B= {" << b.timing << ";" << b.value << "}.\n";
            //std::cout << "["<<time<<"]H = "<<m_height<<" !\n";
        }
        glm::vec3 m_center;
        float m_begin;
        float m_end;
        float m_distance;
        float m_radPerSec;
        float m_height;
        std::vector<HeightPair> heights;
};

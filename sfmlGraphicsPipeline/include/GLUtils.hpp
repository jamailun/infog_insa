#ifndef GL_UTILS_HPP
#define GL_UTILS_HPP

#include "Renderable.hpp"
#include "Camera.hpp"
#include "lighting/Material.hpp"
#include "gl_helper.hpp"

#include <array>
#include <vector>
#include <utility>
#include <glm/glm.hpp>

struct OpenGL_locations {
    int positionLocation;
    int colorLocation;
    int normalLocation;
    int modelLocation;
    int nitLocation;
    int texcoordLocation;
    int camMatLocation;
    int camVecPos;
    int camVecForward;
    int texsamplerLocation;
    int timeLoc;
};

void openGL_basic(
    ShaderProgramPtr shader, Camera* camera, MaterialPtr material, glm::mat4 modelMatrix
);

void openGL_all(
    ShaderProgramPtr shader, Camera* camera, MaterialPtr material, bool textured, glm::mat4 modelMatrix,
    std::vector< unsigned int > m_indices,
    unsigned int m_pBuffer, unsigned int m_cBuffer, unsigned int m_nBuffer,
    unsigned int m_iBuffer, unsigned int m_tBuffer, unsigned int m_texId
);

#endif //UTILS

#ifndef REAL_CONSTANT_FORCE_FIELD_HPP
#define REAL_CONSTANT_FORCE_FIELD_HPP

#include <vector>
#include "ForceField.hpp"
#include "Particle.hpp"

class RealConstantForceField {
    public:
        RealConstantForceField(const glm::vec3& force);
        const glm::vec3& getForce();
        void setForce(const glm::vec3& force);
        void apply(ParticlePtr particle);
    private:
        glm::vec3 m_force;
};

typedef std::shared_ptr<RealConstantForceField> RealConstantForceFieldPtr;

#endif


#ifndef FULL_RENDERABLE_HPP
#define FULL_RENDERABLE_HPP

#include "HierarchicalRenderable.hpp"
#include "KeyframeCollection.hpp"
#include "./lighting/Material.hpp"
#include "GLUtils.hpp"

#include <string>
#include <vector>
#include <glm/glm.hpp>

class FullRenderable : public HierarchicalRenderable {
    public:
        ~FullRenderable();
        FullRenderable(
            ShaderProgramPtr program,
            const std::string& mesh_filename,
            const std::string& texture_filename
        );
        void setMaterial(const MaterialPtr& material);
   
        void addLocalTransformKeyframe( const GeometricTransformation& transformation, float time );
        void addParentTransformKeyframe( const GeometricTransformation& transformation, float time );
        
        int debug_id = 0;
    private:
        void do_draw();
        void do_animate( float time );

        bool textured;

        std::vector< glm::vec3 > m_positions;
        std::vector< glm::vec3 > m_normals;
        std::vector< glm::vec4 > m_colors;
        std::vector< glm::vec2 > m_texCoords;
        std::vector< unsigned int > m_indices;

        unsigned int m_pBuffer; //vertices
        unsigned int m_cBuffer; //colors
        unsigned int m_nBuffer; //normals
        unsigned int m_iBuffer; //indices
        unsigned int m_tBuffer; //texture coordinates
        unsigned int m_texId;
        //unsigned int m_mipmapOption;

        MaterialPtr m_material;

        KeyframeCollection m_localKeyframes;
        KeyframeCollection m_parentKeyframes;
};

typedef std::shared_ptr<FullRenderable> FullRenderablePtr;


class ContainerRenderable : public HierarchicalRenderable {
    public:
        ContainerRenderable(ShaderProgramPtr shaderProgram): HierarchicalRenderable(shaderProgram)
        {}
   
        void addLocalTransformKeyframe( const GeometricTransformation& transformation, float time ) {
            m_localKeyframes.add( transformation, time );
        }
        void addParentTransformKeyframe( const GeometricTransformation& transformation, float time ) {
            m_parentKeyframes.add( transformation, time );
        }
    private:
        void do_draw() {}
        void do_animate( float time ) {
            if(!m_localKeyframes.empty()) {
                setLocalTransform( m_localKeyframes.interpolateTransformation( time ) );
            }
            if(!m_parentKeyframes.empty()) {
                setParentTransform( m_parentKeyframes.interpolateTransformation( time ) );
            }
        }

        KeyframeCollection m_localKeyframes;
        KeyframeCollection m_parentKeyframes;
};

typedef std::shared_ptr<ContainerRenderable> ContainerRenderablePtr;

#endif

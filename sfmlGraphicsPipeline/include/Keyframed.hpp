#ifndef KEYFRAMED_HPP
#define KEYFRAMED_HPP

#include "HierarchicalRenderable.hpp"
#include "KeyframeCollection.hpp"

#include <glm/glm.hpp>

class GeometricTransformation;

class Keyframed {
    public:
        /**
        * \brief Add a keyframe for the local transformation of the renderable.
        *
        * Add a keyframe to m_localKeyframes described by a geometric transformation and a time.
        * \param transformation The geometric transformation of the keyframe.
        * \param time The time of the keyframe.
        */
        void addLocalTransformKeyframe( const GeometricTransformation& transformation, float time );

        /**
        * \brief Add a keyframe for the parent transformation of the renderable.
        *
        * Add a keyframe to m_parentKeyframes described by a geometric transformation and a time.
        * \param transformation The geometric transformation of the keyframe.
        * \param time The time of the keyframe.
        */
        void addParentTransformKeyframe( const GeometricTransformation& transformation, float time );
        friend void animate_frame(float time);

    private:
        KeyframeCollection m_localKeyframes;
        KeyframeCollection m_parentKeyframes;
};

typedef std::shared_ptr<Keyframed> KeyframedPtr;

#endif
